﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{
    bool showingMenu = false;

    public GameObject menu;
    public GameObject dead;
    public GameObject levelUp;
     
    public bool isDead =false;

    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
        dead.SetActive(false);
        levelUp.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Submit") && !isDead)
        {
            showingMenu = !showingMenu;
            menu.SetActive(showingMenu);
            if (showingMenu)
            {
                Time.timeScale = 0;
                EventSystem.current.SetSelectedGameObject(menu);
            }
            else
            {
                Time.timeScale = 1;
            }

        }
        if (showingMenu && Input.GetButtonDown("Interact"))
            OnExit();

    }
    public void OnDeath()
    {
        isDead = true;
        dead.SetActive(true);

    }
    public void OnExit()
    {
        SceneManager.LoadScene(0);
    }
}
