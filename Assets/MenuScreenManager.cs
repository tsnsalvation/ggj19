﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuScreenManager : MonoBehaviour
{
    public Button start;

    // Start is called before the first frame update
    void Start()
    {
        EventSystem.current.SetSelectedGameObject(start.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnExit()
    {
        Application.Quit();
    }
    public void OnCredits()
    {

    }
    public void OnStart()
    {
        SceneManager.LoadScene(1);
    }

}
