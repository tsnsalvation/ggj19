﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    public GameObject target;
    NavMeshAgent agent;

    public float aggroRadius;

    public List<Transform> waypoints;
    int waypointIndex;
    public Transform currentWaypoint;
    public Transform playerAgent;

    public float skinWidth =0.5f;

    public float attackRange = 1f;
    bool attacking = false;
    bool hasHit = false;
    float attackTimer;
    public float hitTime = 0.7f;

    public int attackDamage;

    Animator anim;
    public AnimationClip attackAnimation;

    bool stunned;
    public float stunTime;
    float stunTimer;

    public AudioSource walk;
    public AudioSource attack;


    float baseSpeed = 3.5f;
    

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        playerAgent = GameObject.Find("Player").transform;
        waypointIndex = waypoints.FindIndex(x => x == currentWaypoint);
        anim = GetComponent<Animator>();
        hitTime = 0.7f * attackAnimation.length;
        GameObject.Find("GameController").GetComponent<GameManager>().golems.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        agent.destination = target.transform.position;
        if (Vector3.Distance(transform.position, playerAgent.position) < aggroRadius)
        {
            agent.destination = target.transform.position;
            if (Vector3.Distance(transform.position, playerAgent.position) < attackRange && !attacking)
            {
                anim.SetTrigger("Attack");
                attacking = true;
                hasHit = false;
                attackTimer = 0;
                walk.Stop();
                attack.Play();
            }
        }
        else
        {
            agent.destination = currentWaypoint.position;
            if (Vector3.Distance(transform.position, currentWaypoint.position) < skinWidth)
            {
                NextWaypoint();
            }
        }
        if (attacking)
        {
            attackTimer += Time.deltaTime;
            if (!hasHit && attackTimer > hitTime)
            {
                hasHit = true;
                var health = playerAgent.GetComponent<PlayerHealth>();
                health.ChangeHealth(-attackDamage);
                health.PlayHit();

            }
            if (attackTimer > attackAnimation.length)
            {
                attacking = false;
                walk.Play();
            }
        }
        if (stunned)
        {
            stunTimer += Time.deltaTime;
            if (stunTimer > stunTime)
                Recover();
        }
    }

    void NextWaypoint()
    {
        if (waypointIndex == waypoints.Count - 1)
        {
            currentWaypoint = waypoints[0];
            waypointIndex = 0;
        }
        else
        {
            waypointIndex++;
            currentWaypoint = waypoints[waypointIndex];
        }
    }

    public void Stunned()
    {
        stunned = true;
        agent.speed = 0;
        anim.SetBool("Idle",true);
        stunTimer = 0f;
        walk.Stop();
    }
    void Recover()
    {
        stunned = false;
        agent.speed = baseSpeed;
        anim.SetBool("Idle", false);
        walk.Play();
    }
}
