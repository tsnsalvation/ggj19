﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SporeDamage : MonoBehaviour
{
    public float tickTime = 1.5f;
    public float lastTick;

    public int baseDamageAmount;
    public int damageAmount;
    public ParticleSystem particles;
    int baseParticleCount = 1500;
    

    public float rainSuppressionThreshold;

    private void Start()
    {
        var controller = GameObject.Find("GameController");
        //weatherControl = controller.GetComponent<WeatherController>();
        GameObject.Find("GameController").GetComponent<GameManager>().mushrooms.Add(this);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Time.time > lastTick + tickTime)
            {
                if (damageAmount > baseDamageAmount * 0.1f)
                {
                    var health = other.GetComponent<PlayerHealth>();
                    health.ChangeHealth(-damageAmount);
                    health.PlayCough();
                    lastTick = Time.time;
                }
            }
        }
    }

    private void Update()
    {
    }
    public void ReduceBloom(float reductionFactor)
    {

        var maxpParticle = particles.main;
        maxpParticle.maxParticles = (int)(baseParticleCount * reductionFactor);

    }
}
