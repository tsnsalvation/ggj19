﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerDodge : MonoBehaviour
{
    bool dodging;
    bool dodgeRecharge;

    float dodgeDelayTime;
    public float delayToNextDodge;

    float dodgeTime;
    public float maxDodgeTime;

    public ThirdPersonUserControl controller;
    public ThirdPersonCharacter character;

    public float dodgeDistance;
    Vector3 dodgeOrigin;
    Vector3 dodgeTarget;

    Animator anim;

    void Start()
    {
        controller = GetComponent<ThirdPersonUserControl>();
        character = GetComponent<ThirdPersonCharacter>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetButton("Dodge"))
        {
            if (!dodging && !dodgeRecharge)
            {
                dodging = true;
                controller.enabled = false;
                character.enabled = false;
                dodgeOrigin = transform.position;
                dodgeTarget = transform.position + (transform.forward * dodgeDistance);
                anim.speed = 0;
            }
        }
        if (dodging)
        {
            dodgeTime += Time.deltaTime;
            transform.position = Vector3.Lerp(dodgeOrigin, dodgeTarget, dodgeTime / maxDodgeTime);
            if (dodgeTime > maxDodgeTime)
            {
                StopDodging();
            }
        }
        if (dodgeRecharge)
        {
            dodgeDelayTime += Time.deltaTime;
            if (dodgeDelayTime > delayToNextDodge)
            {
                dodgeRecharge = false;
                dodgeDelayTime = 0;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != "NPC")
            StopDodging();
    }

    void StopDodging()
    {
        dodging = false;
        dodgeRecharge = true;
        controller.enabled = true;
        character.enabled = true;
        dodgeTime = 0;
        anim.speed = 1;
    }
}
