﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int currentHealth;
    int maxHealth = 100;

    public AudioSource coughSound;
    public AudioSource hitSound;

    public MenuManager menu;

    public Text health;

    public void ChangeHealth(int value)
    {
        currentHealth = Mathf.Min(currentHealth + value, maxHealth);
        health.text = $"Health: {currentHealth}%";
        if (currentHealth <= 0)
            OnDeath();
    }

    void OnDeath()
    {
        menu.OnDeath();
    }
    public void PlayCough()
    {
        if (!coughSound.isPlaying)
            coughSound.Play();
    }
    public void PlayHit()
    {
        hitSound.Play();
    }
}
