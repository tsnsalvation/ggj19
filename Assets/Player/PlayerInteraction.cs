﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public Collider interactObject;
    public Collider interactNPC;
    bool interacting;

    public float interactionTimer;
    float currentTimer;

    Animator anim;

    public GameManager manager;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Interact"))
        {
            if (interactObject != null && !interacting)
            {
                interacting = true;
                currentTimer = 0;
                anim.SetTrigger("Pickup");
                interactObject.tag = "Depleted";
                interactObject.transform.GetChild(0).gameObject.SetActive(false);
                interactObject = null;
            }
            if (interactNPC != null)
            {
                if (manager.suppliesCollected >= 6)
                {
                    currentTimer = 0;
                    interactNPC.tag = "Depleted";
                    interactNPC.transform.GetChild(0).gameObject.SetActive(false);
                    interactNPC.transform.gameObject.GetComponent<QuestReturn>().GoLive();
                    interactNPC = null;
                    gameObject.GetComponent<PlayerDodge>().delayToNextDodge = 1f;
                    manager.suppliesCollected = -1;
                    manager.OnSuppliesCollected();
                }
            }
        }

        if (interacting)
        {
            currentTimer += Time.deltaTime;
            if (currentTimer > interactionTimer)
            {
                currentTimer = 0;
                interacting = false;
                manager.OnSuppliesCollected();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Interaction")
            interactObject = other;
        if (other.tag == "NPC")
            interactNPC = other;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Interaction")
            interactObject = null;
        if (other.tag == "NPC")
            interactNPC = null;
    }
}
