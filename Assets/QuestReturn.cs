﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestReturn : MonoBehaviour
{
    public MenuManager manager;

    float fade = 3.0f;
    float time;
    public bool live = false;


    // Update is called once per frame
    void Update()
    {
        if (!live)
            return;
        time += Time.deltaTime;
        if (time > fade)
            manager.levelUp.SetActive(false);
    }
    public void GoLive()
    {
        live = true;
        manager.levelUp.SetActive(true);
    }
}
