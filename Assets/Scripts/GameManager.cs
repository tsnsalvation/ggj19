﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.RainMaker;

public class GameManager : MonoBehaviour
{

    public List<AIController> golems;
    public List<SporeDamage> mushrooms;

    public List<GameStage> gameStages;
    public int currentStage = 0;
    public float gameProgressionTimer;

    public RainScript rain;

    public int suppliesCollected;
    public Text supplies;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameProgressionTimer += Time.deltaTime;
        if (gameProgressionTimer > gameStages[currentStage].time)
        {
            if (currentStage >= gameStages.Count - 1)
                currentStage = 0;
            else
                currentStage++;
            gameProgressionTimer = 0;
            switch (gameStages[currentStage].rain)
            {
                case RainState.none:
                    RainStop();
                    break;
                case RainState.light:
                    LightRainStart();
                    break;
                case RainState.medium:
                    MediumRainStart();
                    break;
                case RainState.heavy:
                    HeavyRainStart();
                    break;
            }
            switch (gameStages[currentStage].thunder)
            {
                case ThunderState.none:
                    
                    break;
                case ThunderState.light:
                    
                    break;
                case ThunderState.medium:
                    
                    break;
                case ThunderState.heavy:
                    
                    break;
            }
        }
    }

    public void RainStop()
    {
        rain.RainIntensity = 0;
        for (int i = 0; i < mushrooms.Count; i++)
        {
            mushrooms[i].damageAmount = mushrooms[i].baseDamageAmount;
            mushrooms[i].ReduceBloom(1);
        }
    }
    public void LightRainStart()
    {
        rain.RainIntensity = 0.25f;
        for (int i = 0; i < mushrooms.Count; i++)
        {
            mushrooms[i].damageAmount = (int)(mushrooms[i].baseDamageAmount * 0.7f);
            mushrooms[i].ReduceBloom(0.7f);
        }
    }
    public void MediumRainStart()
    {
        rain.RainIntensity = 0.6f;
        for (int i = 0; i < mushrooms.Count; i++)
        {
            mushrooms[i].damageAmount = (int)(mushrooms[i].baseDamageAmount * 0.4f);
            mushrooms[i].ReduceBloom(0.3f);
        }
    }
    public void HeavyRainStart()
    {
        rain.RainIntensity = 1;
        for (int i = 0; i < mushrooms.Count; i++)
        {
            mushrooms[i].damageAmount = 0;
            mushrooms[i].ReduceBloom(0);
        }
    }
    public void ThunderClap()
    {
        for (int i = 0; i < golems.Count; i++)
        {
            golems[i].Stunned();
        }
    }
    public void OnSuppliesCollected()
    {
        suppliesCollected++;
        supplies.text = $"Supplies: {suppliesCollected}";
    }
}
[System.Serializable]
public class GameStage
{
    public float time;
    public RainState rain;
    public ThunderState thunder;

}
public enum RainState
{
    none,
    light,
    medium,
    heavy
}
public enum ThunderState
{
    none,
    light,
    medium,
    heavy
}
