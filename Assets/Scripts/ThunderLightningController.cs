﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderLightningController : MonoBehaviour
{
    public float minFlashTime = 0.5f;
    public float strikeChanceThreshold = 0.5f;
    public Light lightningLight;

    float lastTime = 0;

    public bool striking;
    float startStrikeTime;
    public float strikeDuration = 1.5f;
    public float totalStrikeTime = 1.5f;
    public float nextStrike =1f;

    public float delayBetweenStrikes = 5f;

    public List<AudioSource> thunderSounds;

    public List<AIController> golems;
    public List<SporeDamage> mushrooms;

    GameManager manager;

    private void Start()
    {
        lightningLight.enabled = false;
        manager = GetComponent<GameManager>();
    }

    void Update()
    {
        if ( Time.time > nextStrike  && !striking)
        {
            striking = true;
            nextStrike = Time.time + delayBetweenStrikes + (Random.value * 3);
            startStrikeTime = Time.time;
            totalStrikeTime = 0.2f + (Random.value * strikeDuration);
        }

        if (striking)
        {
            if ((Time.time - lastTime) > minFlashTime)
            {
                if (Random.value > strikeChanceThreshold)
                {
                    lightningLight.enabled = true;
                }
                else
                {
                    lightningLight.enabled = false;
                }
                lastTime = Time.time;
            } 
            if (Time.time > startStrikeTime + totalStrikeTime)
            {
                AndTheThunderRolled();
                striking = false;
                lightningLight.enabled = false;
            }
        }
    }
    void AndTheThunderRolled()
    {
        var sound = Random.Range(0, thunderSounds.Count);
        thunderSounds[sound].Play();
        manager.ThunderClap();
    }
}
